package com.example.submenu.model

class Transactions(
    var bankName: String?,
    var date: String?,
    var amount: String?,
    var status: String?
)
