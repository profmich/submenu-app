package com.example.submenu.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.submenu.R
import com.example.submenu.model.Transactions
import com.example.submenu.swipe.SwipeLayout

class TransactionAdapter(private val transactionList: List<Transactions>) :
    RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bank.text = transactionList[position].bankName
        holder.date.text = transactionList[position].date
        holder.amount.text = transactionList[position].amount
        holder.status.text = transactionList[position].status

        holder.refund.setOnClickListener {
            Toast.makeText(holder.amount.context, "${holder.amount.text} Refunded", Toast.LENGTH_SHORT).show()
        }
        holder.reprocess.setOnClickListener {
            Toast.makeText(holder.amount.context, "${holder.amount.text} Reprocessed", Toast.LENGTH_SHORT).show()
        }
        holder.delete.setOnClickListener {
            Toast.makeText(holder.amount.context, "${holder.amount.text} Deleted", Toast.LENGTH_SHORT).show()
        }


        holder.swipeLayout.surfaceView!!.setOnClickListener {
            Toast.makeText(holder.amount.context, "${holder.bank.text} Clicked", Toast.LENGTH_SHORT).show()
        }
        holder.swipeLayout.surfaceView!!.setOnLongClickListener {
            Toast.makeText(holder.amount.context, "${holder.bank.text} Long Clicked", Toast.LENGTH_SHORT).show()
            true
        }
    }

    override fun getItemCount(): Int {
        return transactionList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var bank: TextView = itemView.findViewById(R.id.bank)
        var date: TextView = itemView.findViewById(R.id.date)
        var amount: TextView = itemView.findViewById(R.id.amount)
        var status: TextView = itemView.findViewById(R.id.status)
        var delete: ImageView = itemView.findViewById(R.id.delete)
        var refund: TextView = itemView.findViewById(R.id.refund)
        var reprocess: TextView = itemView.findViewById(R.id.reprocess)
        var swipeLayout : SwipeLayout = itemView.findViewById(R.id.swipeLayout)

        init {
            swipeLayout.showMode = SwipeLayout.ShowMode.PullOut

            swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.leftMenu) as LinearLayout)
            swipeLayout.addDrag(SwipeLayout.DragEdge.Right,swipeLayout.findViewById(R.id.rightMenu) as LinearLayout)

        }

    }

}
