package com.example.submenu.ui.transactions

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.submenu.Repository.TransactionRepository
import com.example.submenu.model.Transactions

class TransactionsViewModel : ViewModel() {

    var transactionRepository: TransactionRepository? = null
    private var mutableLiveDataTransactionList: MutableLiveData<List<Transactions>>? = null

   fun init() {
       transactionRepository = TransactionRepository.instance
        mutableLiveDataTransactionList = transactionRepository!!.getTransactionData()

    }

    fun getMutableTransactionList() : MutableLiveData<List<Transactions>>{
        return mutableLiveDataTransactionList!!
    }
}