package com.example.submenu.ui.transactions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.submenu.R
import com.example.submenu.adapter.TransactionAdapter

class TransactionsFragment : Fragment() {

    private lateinit var transactionsViewModel: TransactionsViewModel
    private lateinit var transactionAdapter: TransactionAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        transactionsViewModel =
            ViewModelProviders.of(this).get(TransactionsViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_home, container, false)


        transactionsViewModel.init()

        initializeRecycler(root)

        transactionsViewModel.getMutableTransactionList().observe(this, Observer {
            transactionAdapter.notifyDataSetChanged()
        })

        return root
    }

    private fun initializeRecycler(root : View) {
        val recyclerView : RecyclerView = root.findViewById(R.id.transactionRecycler)

        transactionAdapter = TransactionAdapter(transactionsViewModel.getMutableTransactionList().getValue()!!)
        recyclerView.setAdapter(transactionAdapter)
        recyclerView.setLayoutManager(LinearLayoutManager(activity, RecyclerView.VERTICAL, false))

    }
}