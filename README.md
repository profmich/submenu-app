# SubMenu App

A Recycler View implemented using viewModel and LiveData with each item having a submenu on the left and right.


## Activity
- MainActivity

## Fragment
- Home Fragment 
- Transfer Fragment 

## ViewModel
- Home ViewModel
- Transfer ViewModel 

## Repository
Transaction Repository

## Transaction Adapter
Recycler Adapter, also holds reactions of submenu clicks

## Swipe Layout
Handles the recycler item Swipes
